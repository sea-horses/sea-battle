"""sea_horses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from sea_battle.views import ranking2
from rest_framework import routers
from sea_battle.views import UserViewSet, GameViewSet, ShipViewSet, RankingViewSet, AttackViewSet


from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'games', GameViewSet)
router.register(r'ships', ShipViewSet)
router.register(r'rankings', RankingViewSet)
router.register(r'attacks', AttackViewSet)

schema_view = get_schema_view(
    openapi.Info(
        title='Sea Horses API',
        default_version='v1'
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # path('', views.index, name='index'),
    path('ranking2/', ranking2, name='ranking2'),
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path(r'api/v1/swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]




