import random


class Sea(object):
    board = []

    def painting_sea(self):
        for number in range(10):
            self.board.append(["~"] * 10)

    def random_direction(self):
        random_number = random.randint(1, 2)
        if random_number == 1:
            return 'row'
        return 'col'

    def searching_free_slots(self, direction, len_ship):
        ship_coordinates = []
        valid_slots = 0
        random_slot = random.randint(0, 9)
        ship_start_random = random.randint(0, 9 - len_ship)

        while valid_slots < len_ship:
            if direction == 'row' and self.board[ship_start_random][random_slot] == '~':

                ship_start_random += 1
                ship_coordinates.append({ship_start_random - 1: random_slot})
                valid_slots += 1
            elif direction == 'col' and self.board[random_slot][ship_start_random] == '~':
                ship_start_random += 1
                ship_coordinates.append({random_slot: ship_start_random - 1})
                valid_slots += 1
            else:
                ship_coordinates = []
                random_slot = random.randint(0, 9)
                ship_start_random = random.randint(0, 9 - len_ship)
                valid_slots = 0

        return ship_coordinates


class Warship(Sea):
    used_ships = []

    def unrepeated_ship(self):
        while len(self.used_ships) < 10:
            len_ship = random.randint(1, 10)
            if len_ship not in self.used_ships:
                self.used_ships.append(len_ship)
                if 0 < len_ship < 5:
                    return 1
                elif 4 < len_ship < 8:
                    return 2
                elif 7 < len_ship < 10:
                    return 3
                elif len_ship == 10:
                    return 4

    def placing_ships_and_dots(self, coordinates):
        dot_coordinates = [[1, 1], [1, 0], [0, 1], [-1, -1], [-1, 0], [0, -1], [-1, 1], [1, -1]]
        len_ship = len(coordinates)

        for coord in coordinates:
            for row, col in coord.items():
                self.board[row][col] = str(len_ship)
                for index in dot_coordinates:
                    if row + index[0] < 0 or row + index[0] > 9 or col + index[1] < 0 or col + index[1] > 9:
                        continue
                    elif self.board[row + index[0]][col + index[1]] != '~':
                        continue
                    else:
                        self.board[row + index[0]][col + index[1]] = '.'


def build_ranking_1():

    a = Sea()
    b = Warship()

    a.painting_sea()
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))
    b.placing_ships_and_dots(a.searching_free_slots(a.random_direction(), b.unrepeated_ship()))

    # for x in a.board:
    #     print(" ".join(x))
    #
    # print("\n")

    ship_1 = {}
    ship_2 = {}
    ship_3 = {}
    ship_4 = {}

    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "4":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "4" or a.board[row][col - 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                    a.board[row][col + 2] = "~"
                    a.board[row][col + 3] = "~"
                else:
                    a.board[row + 1][col] = "~"
                    a.board[row + 2][col] = "~"
                    a.board[row + 3][col] = "~"

                ship_4 = {"row": row,
                          "col": col,
                          "orientation": orientation}
            col += 1
        row += 1

    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "3":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "3" or a.board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                    a.board[row][col + 2] = "~"
                else:
                    a.board[row + 1][col] = "~"
                    a.board[row + 2][col] = "~"

                ship_3[0] = {"row": row,
                             "col": col,
                             "orientation": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1

    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "3":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "3" or a.board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                    a.board[row][col + 2] = "~"
                else:
                    a.board[row + 1][col] = "~"
                    a.board[row + 2][col] = "~"

                ship_3[1] = {"row": row,
                             "col": col,
                             "orientation": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "2" or a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                else:
                    a.board[row + 1][col] = "~"

                ship_2[0] = {"row": row,
                             "col": col,
                             "orientation": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "2" or a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                else:
                    a.board[row + 1][col] = "~"

                ship_2[1] = {"row": row,
                             "col": col,
                             "orientation": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if a.board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if a.board[row][col + 1] == "2" or a.board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                a.board[row][col] = "~"

                if orientation == "horizon":
                    a.board[row][col + 1] = "~"
                else:
                    a.board[row + 1][col] = "~"

                ship_2[2] = {"row": row,
                             "col": col,
                             "orientation": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "1":

                a.board[row][col] = "~"

                ship_1[0] = {"row": row,
                             "col": col}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "1":

                a.board[row][col] = "~"

                ship_1[1] = {"row": row,
                             "col": col}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "1":

                a.board[row][col] = "~"

                ship_1[2] = {"row": row,
                             "col": col}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in a.board:
        col = 0
        for y in x:

            if y == "1":

                a.board[row][col] = "~"

                ship_1[3] = {"row": row,
                             "col": col}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1

    # print(ship_4)
    # print(ship_3)
    # print(ship_2)
    # print(ship_1)

    return ship_4, ship_3, ship_2, ship_1


def build_ranking_2():
    board = []
    ship_1 = {}
    ship_2 = {}
    ship_3 = {}
    ship_4 = {}

    for number in range(10):
        board.append(["~"] * 10)

    col = random.choice([0, 1, 2, 3, 4, 5, 6, 9])
    if col == 0 or col == 9:
        row = random.randint(0, 6)
        board[row + 1][col] = "4"
        board[row + 2][col] = "4"
        board[row + 3][col] = "4"
    else:
        row = random.choice([0, 9])
        board[row][col + 1] = "4"
        board[row][col + 2] = "4"
        board[row][col + 3] = "4"
    board[row][col] = "4"


    exitFlag = True
    while exitFlag:
        col = random.choice([0, 1, 2, 3, 4, 5, 6, 7, 9])
        if col == 0 or col == 9:
            row = random.randint(0, 7)
            if row == 0 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row + 3][col] == "~" and board[0][1] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 0 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row + 3][col] == "~" and board[0][8] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 7 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[9][1] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 7 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[9][8] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[row + 3][col] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
        else:
            row = random.choice([0, 9])
            if col == 7 and row == 0:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~" and board[1][9] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
            elif col == 7 and row == 9:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~" and board[8][9] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col + 3] == "~" and board[row][col - 1] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.choice([0, 1, 2, 3, 4, 5, 6, 7, 9])
        if col == 0 or col == 9:
            row = random.randint(0, 7)
            if row == 0 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row + 3][col] == "~" and board[0][1] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 0 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row + 3][col] == "~" and board[0][8] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 7 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[9][1] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            elif row == 7 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[9][8] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~" and board[row + 3][col] == "~":
                    board[row][col] = "3"
                    board[row + 1][col] = "3"
                    board[row + 2][col] = "3"
                else:
                    continue
        else:
            row = random.choice([0, 9])
            if col == 7 and row == 0:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~" and board[1][9] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
            elif col == 7 and row == 9:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~" and board[8][9] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col + 3] == "~" and board[row][col - 1] == "~":
                    board[row][col] = "3"
                    board[row][col + 1] = "3"
                    board[row][col + 2] = "3"
                else:
                    continue
        exitFlag = False


    exitFlag = True
    while exitFlag:
        col = random.randint(0, 9)
        if col == 0 or col == 9:
            row = random.randint(0, 8)
            if row == 0 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 0 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
        else:
            row = random.choice([0, 9])
            if col == 8 and row == 0:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[1][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            elif col == 8 and row == 9:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[8][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.randint(0, 9)
        if col == 0 or col == 9:
            row = random.randint(0, 8)
            if row == 0 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 0 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
        else:
            row = random.choice([0, 9])
            if col == 8 and row == 0:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[1][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            elif col == 8 and row == 9:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[8][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.randint(0, 9)
        if col == 0 or col == 9:
            row = random.randint(0, 8)
            if row == 0 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 0 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[0][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 0:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][1] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            elif row == 8 and col == 9:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row - 1][col] == "~" and \
                        board[9][8] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row + 1][col] == "~" and board[row + 2][col] == "~" and \
                        board[row - 1][col] == "~":
                    board[row][col] = "2"
                    board[row + 1][col] = "2"
                else:
                    continue
        else:
            row = random.choice([0, 9])
            if col == 8 and row == 0:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[1][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            elif col == 8 and row == 9:
                if board[row][col] == "~" and board[row][col + 1] == "~" and \
                        board[row][col - 1] == "~" and board[8][9] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
            else:
                if board[row][col] == "~" and board[row][col + 1] == "~" and board[row][col + 2] == "~" and \
                        board[row][col - 1] == "~":
                    board[row][col] = "2"
                    board[row][col + 1] = "2"
                else:
                    continue
        exitFlag = False


    exitFlag = True
    while exitFlag:
        col = random.choice([2, 3, 4, 5, 6, 7])
        row = random.choice([2, 3, 4, 5, 6, 7])
        if board[row + 1][col] == "~" and board[row + 1][col + 1] == "~" and board[row][col + 1] == "~" and \
                board[row - 1][col + 1] == "~" and board[row - 1][col] == "~" and board[row - 1][col - 1] == "~" and \
                board[row][col - 1] == "~" and board[row + 1][col - 1] == "~" and board[row][col] == "~":
            board[row][col] = "1"
            # print(str(row) + " " + str(col))
        else:
            continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.choice([2, 3, 4, 5, 6, 7])
        row = random.choice([2, 3, 4, 5, 6, 7])
        if board[row + 1][col] == "~" and board[row + 1][col + 1] == "~" and board[row][col + 1] == "~" and \
                board[row - 1][col + 1] == "~" and board[row - 1][col] == "~" and board[row - 1][col - 1] == "~" and \
                board[row][col - 1] == "~" and board[row + 1][col - 1] == "~" and board[row][col] == "~":
            board[row][col] = "1"
            # print(str(row) + " " + str(col))
        else:
            continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.choice([2, 3, 4, 5, 6, 7])
        row = random.choice([2, 3, 4, 5, 6, 7])
        if board[row + 1][col] == "~" and board[row + 1][col + 1] == "~" and board[row][col + 1] == "~" and \
                board[row - 1][col + 1] == "~" and board[row - 1][col] == "~" and board[row - 1][col - 1] == "~" and \
                board[row][col - 1] == "~" and board[row + 1][col - 1] == "~" and board[row][col] == "~":
            board[row][col] = "1"
            # print(str(row) + " " + str(col))
        else:
            continue
        exitFlag = False

    exitFlag = True
    while exitFlag:
        col = random.choice([2, 3, 4, 5, 6, 7])
        row = random.choice([2, 3, 4, 5, 6, 7])
        if board[row + 1][col] == "~" and board[row + 1][col + 1] == "~" and board[row][col + 1] == "~" and \
                board[row - 1][col + 1] == "~" and board[row - 1][col] == "~" and board[row - 1][col - 1] == "~" and \
                board[row][col - 1] == "~" and board[row + 1][col - 1] == "~" and board[row][col] == "~":
            board[row][col] = "1"
            # print(str(row) + " " + str(col))
        else:
            continue
        exitFlag = False

    # print(str(row) + " " + str(col))
    # for x in board:
    #     print(" ".join(x))

    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "4":

                if col + 1 == 10:
                    if board[row][col - 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "4" or board[row][col - 1] == "4":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    board[row][col + 2] = "~"
                    board[row][col + 3] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    board[row + 2][col] = "~"
                    board[row + 3][col] = "~"
                    orientation = 1

                # ship_4 = {"row": row,
                #           "col": col,
                #           "orientation": orientation}

                ship_4 = {"x": col,
                          "y": row,
                          "isVertical": orientation}
            col += 1
        row += 1

    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "3":

                if col + 1 == 10:
                    if board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "3" or board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    board[row][col + 2] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    board[row + 2][col] = "~"
                    orientation = 1

                # ship_3[0] = {"row": row,
                #              "col": col,
                #              "orientation": orientation}
                ship_3[0] = {"x": col,
                             "y": row,
                             "isVertical": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1

    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "3":

                if col + 1 == 10:
                    if board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "3" or board[row][col - 1] == "3":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    board[row][col + 2] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    board[row + 2][col] = "~"
                    orientation = 1

                # ship_3[1] = {"row": row,
                #              "col": col,
                #              "orientation": orientation}
                ship_3[1] = {"x": col,
                             "y": row,
                             "isVertical": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "2" or board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    orientation = 1

                # ship_2[0] = {"row": row,
                #              "col": col,
                #              "orientation": orientation}
                ship_2[0] = {"x": col,
                             "y": row,
                             "isVertical": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "2" or board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    orientation = 1

                # ship_2[1] = {"row": row,
                #              "col": col,
                #              "orientation": orientation}
                ship_2[1] = {"x": col,
                             "y": row,
                             "isVertical": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "2":

                if col + 1 == 10:
                    if board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                elif col - 1 == -1:
                    if board[row][col + 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                else:
                    if board[row][col + 1] == "2" or board[row][col - 1] == "2":
                        orientation = "horizon"
                    else:
                        orientation = "vertical"

                board[row][col] = "~"

                if orientation == "horizon":
                    board[row][col + 1] = "~"
                    orientation = 0
                else:
                    board[row + 1][col] = "~"
                    orientation = 1

                # ship_2[2] = {"row": row,
                #              "col": col,
                #              "orientation": orientation}
                ship_2[2] = {"x": col,
                             "y": row,
                             "isVertical": orientation}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "1":

                board[row][col] = "~"

                # ship_1[0] = {"row": row,
                #              "col": col}
                ship_1[0] = {"x": col,
                             "y": row}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "1":

                board[row][col] = "~"

                # ship_1[1] = {"row": row,
                #              "col": col}
                ship_1[1] = {"x": col,
                             "y": row}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "1":

                board[row][col] = "~"

                # ship_1[2] = {"row": row,
                #              "col": col}
                ship_1[2] = {"x": col,
                             "y": row}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1


    exitFlag = False
    row = 0
    for x in board:
        col = 0
        for y in x:

            if y == "1":

                board[row][col] = "~"

                # ship_1[3] = {"row": row,
                #              "col": col}
                ship_1[3] = {"x": col,
                             "y": row}
                exitFlag = True
                break

            col += 1
        if exitFlag:
            break
        row += 1

    return ship_4, ship_3, ship_2, ship_1
