from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.


class User(models.Model):

    name = models.CharField(max_length=120, null=True, blank=True, default=None)
    surname = models.CharField(max_length=120, null=True, blank=True, default=None)
    email = models.CharField(max_length=120)
    password = models.CharField(max_length=120)
    note = models.TextField(null=True, blank=True, default=None)
    create_time = models.DateTimeField(auto_now_add=True)
    edit_time = models.DateTimeField(auto_now=True, null=True)


class Ship(models.Model):

    title = models.CharField(max_length=120)


class Game(models.Model):

    title = models.CharField(max_length=120)
    status = models.BooleanField(null=False)
    id_user_1 = models.ForeignKey(User, related_name='user_1', on_delete=models.CASCADE)
    id_user_2 = models.ForeignKey(User, related_name='user_2', on_delete=models.CASCADE)
    computer = models.BooleanField(null=False)
    sound = models.BooleanField(null=True)


class Ranking(models.Model):

    id_game = models.ForeignKey(Game, related_name='ranking', on_delete=models.CASCADE)
    id_ship = models.ForeignKey(Ship, related_name='ship', on_delete=models.CASCADE)
    x = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    y = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    orientation = models.BooleanField(default=False)


class Attack(models.Model):

    id_game = models.ForeignKey(Game, related_name='attack', on_delete=models.CASCADE)
    id_user = models.ForeignKey(User, related_name='user', on_delete=models.CASCADE)
    x = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    y = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(10)])
    status = models.BooleanField(default=False)

