from rest_framework import serializers
from sea_battle.models import *


class GetUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "name", "surname", "email", "password", "note", "create_time", "edit_time"]


class PostUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["name", "email", "password"]


class PatchUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["name", "surname", "email", "password", "note"]


class GetGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ["id", "title", "status", "id_user_1", "id_user_2"]


class PostGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ["title", "status", "id_user_1", "id_user_2"]


class PatchGameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ["status"]


class GetShipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ship
        fields = ["id", "title"]


class PostShipSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ship
        fields = ["title"]


class PostRankingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ranking
        fields = ["id_game", "id_ship", "x", "y", "orientation"]


class PostAttackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Attack
        fields = ["id_game", "id_user", "x", "y", "status"]
