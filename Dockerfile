FROM python:3.7.4
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /sea_horses
COPY . /sea_horses
RUN pip install pip -U && pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["python", "manage.py", "runserver", "0.0.0.0:5000"]
EXPOSE 5000